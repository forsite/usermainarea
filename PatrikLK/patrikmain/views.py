from django.contrib.auth.decorators import login_required
from django.db.models import QuerySet
from django.shortcuts import render, resolve_url
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.views.decorators.debug import sensitive_post_parameters
from django.contrib.auth import (
    REDIRECT_FIELD_NAME, get_user_model, login as auth_login,
    logout as auth_logout, update_session_auth_hash, authenticate,
)
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.conf import settings
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.sites.shortcuts import get_current_site
from django.template.response import TemplateResponse
from django.contrib.auth.forms import (
    AuthenticationForm, PasswordChangeForm, PasswordResetForm, SetPasswordForm,
)
from .models import User, Orders, PromMaterials
from datetime import datetime, date, timedelta
from .forms import AdminUserChangeForm


# Create your views here.

# def mainpage(request):
#     return render(request, "patrikmain/patrik/index.html")


@csrf_exempt
def registrate(request):
    params = request.POST
    user = User.objects.create_user(params['login'], params['email'], params['password'], phone=params['phone'])
    return render(request, 'patrikmain/login.html')


@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='patrikmain/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))

    if request.method == "POST":
        form = authentication_form(request, data=request.POST)
        if form.is_valid():
            # Ensure the user-originating redirection url is safe.
            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)
            # Okay, security check complete. Log the user in.
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    current_site = get_current_site(request)

    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }
    if extra_context is not None:
        context.update(extra_context)

    if current_app is not None:
        request.current_app = current_app

    return TemplateResponse(request, template_name, context)


@login_required
def prom_mater(request):
    prom_list = list(PromMaterials.objects.all())
    # print(type(prom_list))
    result = []
    i = 0
    while i < len(prom_list):
        result.append(prom_list[i:i + 4])
        i += 4
    # print(result)
    return render(request, "patrikmain/prom_mater.html", {'active': 'prom',
                                                          'cur_user': request.user.username,
                                                          'result': result,
                                                          'media': settings.MEDIA_ROOT})


@login_required
def my_orders(request):
    orders_list = Orders.objects.filter(partnerID=User.objects.get(username=request.user.username).id)
    paginate = Paginator(orders_list, 50)
    page = request.GET.get('page')
    try:
        orders = paginate.page(page)
    except PageNotAnInteger:
        orders = paginate.page(1)
    except EmptyPage:
        orders = paginate.page(paginate.num_pages)
    summa = len(orders_list)
    # print(summa)
    return render(request, "patrikmain/orders.html", {'active': "my_orders", 'result': orders,
                                                      'cur_user': request.user.username})


@login_required
def my_settings(request):
    cur_user = User.objects.get(username=request.user.username)
    if request.method == "POST":
        user_param = request.POST
        cur_user.username = user_param['username']
        cur_user.first_name = user_param['first_name']
        cur_user.last_name = user_param['last_name']
        cur_user.farth_name = user_param['farth_name']
        cur_user.phone = user_param['phone']
        cur_user.yandex_money = user_param['yandex_money']
        cur_user.web_money = user_param['web_money']
        cur_user.bank_card = user_param['bank_card']
        cur_user.save()
        return render(request, "patrikmain/settings.html", {'result': cur_user,
                                                            'cur_user': cur_user,
                                                            'active': 'settings'})
    else:
        return render(request, "patrikmain/settings.html", {'result': cur_user,
                                                            'cur_user': cur_user,
                                                            'active': 'settings'})


def logout(request):
    auth_logout(request)
    return HttpResponseRedirect('/patrikmain/login')
