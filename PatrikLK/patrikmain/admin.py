from django.contrib import admin
from .models import User, Orders, PromMaterials
from .forms import AdminUserAddForm, AdminUserChangeForm
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _
# Register your models here.

class OrdersAdmin(admin.ModelAdmin):
    list_display = ["id", "orderID","partnerID","orderdate", "orderstatus"]
    ordering = ["id"]

class UserAdmin(BaseUserAdmin):
     list_display = ["id", "username", "email", "is_staff", "is_active", "date_joined", "first_name",
                    "last_name", "farth_name", "phone", "yandex_money", "web_money", "bank_card", "last_login"]
     form = AdminUserChangeForm
     add_form = AdminUserAddForm
     fieldsets = (
        (None, {'fields':('username', 'password')}),
        (_('Personal info'), {'fields': (
            'first_name',
            'last_name',
            'farth_name',
            'email',
            'phone',
            'yandex_money',
            'web_money',
            'bank_card'
        )}),
        (_('Permissions'), {'fields': (
            'is_active',
            'is_staff',
            'is_superuser',
            'groups',
            'user_permissions'
        )}),
        (_('Important dates'), {'fields': (
            'last_login',
            'date_joined'
        )}),
     )
     add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2')
        }),
     )

class PromMaterialAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'view']
    ordering = ['id']

admin.site.register(Orders, OrdersAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(PromMaterials, PromMaterialAdmin)