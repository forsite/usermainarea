from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login$', views.login, name='login'),
    url(r'^prom_mater$', views.prom_mater, name='prom_mater'),
    url(r'^my_orders$', views.my_orders, name='my_orders'),
    url(r'^my_settings$', views.my_settings, name='my_settings'),
    url(r'^logout', views.logout, name='logout'),
    url(r'^registry', views.registrate, name='registry')
]