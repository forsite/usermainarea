from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    farth_name = models.CharField(max_length=64,  verbose_name='Отчество', blank=True)
    phone = models.CharField(max_length=32, verbose_name='Номер телефона')
    yandex_money = models.CharField(max_length=16, verbose_name='Яндекс-кошелек', blank=True)
    web_money = models.CharField(max_length=13, verbose_name='Web money кошелек', blank=True)
    bank_card = models.CharField(max_length=20, verbose_name='Номер карты', blank=True)

class Orders(models.Model):
    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"
        ordering = ["orderID"]

    orderID = models.CharField(max_length=5, unique=True, verbose_name='ID заказа', blank=False)
    partnerID = models.ForeignKey(User, verbose_name='Промокод', limit_choices_to={'is_staff': False})
    orderdate = models.DateField(auto_now=False, auto_now_add=False)
    orderstatus = models.BooleanField(verbose_name='Статус выплаты', default=False, blank=False)

class PromMaterials(models.Model):
    class Meta:
        verbose_name = "Реламный материал"
        verbose_name_plural = "Реламный материалы"

    name = models.CharField(max_length=128, unique=True, blank=False, verbose_name='Название материала')
    view = models.FileField(verbose_name='Изображение', upload_to='promMaterials/')
